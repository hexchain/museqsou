#!/usr/bin/env python2
# vim: set fileencoding=utf-8 :

from __future__ import unicode_literals

import pygtk
pygtk.require20()

import cairo
import gtk
import gobject
import json
import logging

from config import config
from streamer import Streamer
from tsukkomi import Tsukkomi

FORMAT = "%(asctime)s: %(levelname)7s: %(filename)s:%(funcName)s: %(message)s"
logging.basicConfig(level=config.loglevel, format=FORMAT)
logger = logging.getLogger("museqsou")


class MainWindow:

    def add_tsukkomi(self, data):
        data = json.loads(data)
        self.tsukkomi.append(Tsukkomi(data['text'], data['color']))

    def refresh(self):
        self.window.queue_draw()
        return True

    def set_input_shape_mask(self, window):
        window.window.input_shape_combine_region(gtk.gdk.Region(), 0, 0)

    def set_transparent(self, sender, event):
        if not sender.is_composited():
            logger.warning("Compositing disabled - unable to continue")
            return None

        cr = sender.window.cairo_create()
        cr.set_source_rgba(1, 1, 1, 0)
        cr.set_operator(cairo.OPERATOR_SOURCE)
        cr.paint()
        cr.set_operator(cairo.OPERATOR_OVER)

    def paint_tsukkomi(self, sender, event):
        def find_empty_slot():
            candidate = min(self.slots)
            for i in xrange(len(self.slots)):
                if not self.slots[i]:
                    return i
                if self.slots[i] == candidate:
                    position = i

            # Otherwise use the "most empty" slot
            return position

        for t in self.tsukkomi:
            if t.slot == -1:
                t.slot = find_empty_slot()
                self.tk_on_screen += 1
            tx = self.screen_width - t.count * t.speed
            ty = config.font_size * t.slot

            if tx + t.width > self.screen_width:
                self.slots[t.slot] = tx + t.width
            else:
                self.slots[t.slot] = 0

            if tx < -t.width:
                self.tk_on_screen -= 1
                t.goodbye()
                self.tsukkomi.remove(t)
                continue

            cr = sender.window.cairo_create()
            if not t.drawn:
                t.draw()
            cr.set_source_surface(t.surface, tx, ty)
            cr.paint()

            t.count += 1

    def __init__(self):
        self.window = gtk.Window(gtk.WINDOW_POPUP)
        self.darea = gtk.DrawingArea()
        screen = self.window.get_screen()
        self.screen_width = screen.get_width()
        self.screen_height = screen.get_height()
        logger.info("Screen size: %d x %d" %
                    (self.screen_width, self.screen_height))

        colormap = screen.get_rgba_colormap()

        if not colormap:
            logger.warning("Your screen does not support alpha channel.")
            exit(1)

        self.window.set_default_size(self.screen_width, self.screen_height)
        self.window.set_colormap(colormap)

        self.darea.connect("expose-event", self.set_transparent)
        self.darea.connect("expose-event", self.paint_tsukkomi)

        self.window.add(self.darea)
        self.window.show_all()

        self.set_input_shape_mask(self.window)

        gobject.timeout_add(config.refresh_interval, self.refresh)

        self.density = config.density

        self.tsukkomi = []
        self.tk_on_screen = 0
        self.max_comments = int(self.screen_height / config.font_size)
        self.slots = [0 for i in xrange(self.max_comments)]

    def main(self):
        stream = Streamer(callback=self.add_tsukkomi)
        stream.start()

        try:
            gtk.main()
        except KeyboardInterrupt:
            logger.info("KeyboardInterrupt received, exiting")


if __name__ == '__main__':
    main_window = MainWindow()
    main_window.main()
