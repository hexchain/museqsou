#!/usr/bin/env python2
# vim: set fileencoding=utf-8 :

import logging
import requests
import threading
import time
import json

from config import config

FORMAT = "%(asctime)s: %(levelname)7s: %(filename)s:%(funcName)s: %(message)s"
logging.basicConfig(level=config.loglevel, format=FORMAT)
logger = logging.getLogger("streamer")


class Streamer(threading.Thread):

    def __init__(self, callback=None):
        threading.Thread.__init__(self)
        self.daemon = True
        self.name = "streamer"
        self.callback = callback

    def run(self):

        logger.debug("Starting streamer...")

        while True:
            logger.debug("Connecting...")
            connected = False
            while not connected:
                try:
                    data = {'api_key': config.api_key}
                    r = requests.post(config.api_url,
                                      data=data, stream=True)
                    if r.status_code != 200:
                        raise requests.ConnectionError(r.status_code)
                    connected = True
                except requests.ConnectionError as e:
                    logger.warning("Connection failed, error code " +
                                   str(e.message))
                    time.sleep(float(config.retry_interval))

            logger.debug("Connected. Starting iteration")
            for line in r.iter_lines(chunk_size=1):
                try:
                    line = line.strip()
                    if not line:
                        continue
                    line_json = json.loads(line)
                    if "control" in line_json:
                        logger.info("Received control message:" +
                                    line_json["control"])
                        continue
                    logger.debug("received: " + line)
                    if self.callback:
                        self.callback(line)
                except:
                    import traceback
                    traceback.print_exc()
                    logger.error("Error occurred, retrying")
                    time.sleep(float(config.retry_interval))
                    break
