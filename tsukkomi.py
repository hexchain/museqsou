#!/usr/bin/env python2
# vim: set fileencoding=utf-8 :

import pygtk
pygtk.require20()

import cairo
import logging

from math import ceil
from config import config

FORMAT = "%(asctime)s: %(levelname)7s: %(filename)s:%(funcName)s: %(message)s"
logging.basicConfig(level=config.loglevel, format=FORMAT)
logger = logging.getLogger("tsukkomi")


class Tsukkomi():
    class _extents(object):
        pass

    def __init__(self, text, color="ffffff", draw=True):
        def parse_color(strcolor):
            r, g, b = strcolor[:2], strcolor[2:4], strcolor[4:]
            r, g, b = [(int(n, 16)) / 255.0 for n in (r, g, b)]
            return (r, g, b)

        self.r, self.g, self.b = parse_color(color)
        self.text = text
        self.count = 0
        self.slot = -1
        self.extents = self._extents()
        self.drawn = False

        if draw:
            self.draw()

    def draw(self):
        def calculate_size(text):
            surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 0, 0)

            cr = cairo.Context(surface)
            cr.select_font_face(config.font_face)
            cr.set_font_size(config.font_size)
            result = zip(('xb', 'yb', 'width', 'height', 'xa', 'ya'),
                         cr.text_extents(text))
            surface.finish()
            for item in result:
                setattr(self.extents, item[0], int(ceil(item[1])))

        calculate_size(self.text)

        self.width = self.extents.width + 2 * config.outline * len(self.text)
        self.height = self.extents.height + 2 * config.outline

        self.surface = cairo.ImageSurface(cairo.FORMAT_ARGB32,
                                          self.width, self.height)

        cr = cairo.Context(self.surface)
        cr.select_font_face(config.font_face)
        cr.set_font_size(config.font_size)

        cr.move_to(0, -self.extents.yb + config.outline)
        cr.set_source_rgba(self.r, self.g, self.b, 0.9)
        cr.text_path(self.text)
        cr.fill_preserve()
        cr.set_source_rgba(0.5, 0.5, 0.5, 0.9)
        cr.set_line_width(config.outline)
        cr.stroke()
        self.surface.flush()

        self.speed = config.distance * \
            ((config.factor + self.width) /
             (config.factor + config.font_size * 10))

        self.drawn = True

    def goodbye(self):
        self.extents = None
        self.surface.finish()
        self.surface = None
