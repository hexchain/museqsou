#!/usr/bin/env python2
# vim: set fileencoding=utf-8 :

import json


class _config(object):
    pass

config = _config()

with open("config.json", 'r') as f:
    for k, v in json.load(f).items():
        setattr(config, k, v)
